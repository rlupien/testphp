<?php

// Classe de base
class Legume {

   var $comestible;
   var $couleur;
   var $qte;

   function __construct($comestible, $couleur="vert", $qte )
   {
       $this->comestible = $comestible;
       $this->couleur = $couleur;
	   $this->qte = $qte;
   }

   function estComestible()
   {
       return $this->comestible;
   }

   function obtenirCouleur()
   {
       return $this->couleur;
   }
   
   function ajoutItem()
   {
       return $this->qte + ($this->qte + 1);
   }
}
?>
